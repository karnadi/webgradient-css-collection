import { Injectable } from '@angular/core';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free';

@Injectable()
export class AdmobProvider {
  isPrepared:boolean = false;

  ID = {
    banner: 'ca-app-pub-8701867505893591/7465733117',
    interstitial: 'ca-app-pub-8701867505893591/7274161421'
  };

  constructor(private admob: AdMobFree) {

  }

  prepareBanner() {
    let config: AdMobFreeBannerConfig = {
      id: this.ID.banner,
      autoShow: true,
      size: 'SMART_BANNER',
      isTesting: false,
      overlap: false

    }
    this.admob.banner.config(config);
    this.admob.banner.prepare().then(() => {
      console.log('banner prepared')
      // this.admob.banner.show();

    }).catch((e) => {
      console.log(JSON.stringify(e))
    })
  }

  prepareInterstitial() {
    let config: AdMobFreeInterstitialConfig = {
      id: this.ID.interstitial,
      autoShow: false,
      isTesting: false,

    }
    this.admob.interstitial.config(config);
    this.admob.interstitial.prepare().then(() => {
      this.isPrepared = true;
      console.log('interstitial prepared')
    }).catch((e) => {
      console.log(JSON.stringify(e))
    })
  }

  // prepareRewardVideo(){

  // }

  showBanner() {
    this.admob.banner.show().then(() => {
      console.log('banner showed!')
    }).catch((e) => {
      console.log(JSON.stringify(e))
    })
  }

  showInterstitial() {
    this.admob.interstitial.show().then(() => {
      console.log('interstitial showed!');
      // Prepare again after showed
      this.prepareInterstitial()
    }).catch((e) => {
      console.log(JSON.stringify(e))
      this.prepareInterstitial()
    })
  }

  // showRewardVideo(){

  // }

}