import { NgModule } from '@angular/core';
import { PrettyPrintPipe } from './pretty-print/pretty-print';
import { BackgroundPipe } from './background/background';
@NgModule({
	declarations: [PrettyPrintPipe,
    BackgroundPipe],
	imports: [],
	exports: [PrettyPrintPipe,
    BackgroundPipe]
})
export class PipesModule {}
