import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'background',
})
export class BackgroundPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string) {
    let style = "{'"+value.replace(/:\s*/g,"':'").replace(/;\s*/g,"','").trim()+'}'
    let json = (style.slice(0,-3)+'}').replace(/'\s*/g,'"')
    return JSON.parse(json)
  }
}
