import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'prettyPrint',
})
export class PrettyPrintPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(str: string) {
    let replaced = str.replace(/[0-9]/g, '');;
    // let array = replaced.split("<br/>");

    // for(let el of array) {
    //     if(!!el === false) {
    //         array.splice(array.indexOf(el), 1);
    //     }
    // }

    return replaced;
}
}
