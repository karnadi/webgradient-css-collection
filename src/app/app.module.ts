import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';
import { HttpClientModule } from '@angular/common/http';
import { Clipboard } from '@ionic-native/clipboard';
import { Toast } from '@ionic-native/toast';
import { AdmobProvider } from '../providers/admob/admob';
import { AdMobFree } from '@ionic-native/admob-free';
import { Screenshot } from '@ionic-native/screenshot';
import { TestProvider } from '../providers/test/test';
@NgModule({
  declarations: [
    MyApp,
 
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AdmobProvider,
    AdMobFree,
    Clipboard,
    Toast,
    TestProvider,
    AndroidFullScreen,
    Screenshot

  ]
})
export class AppModule {}
