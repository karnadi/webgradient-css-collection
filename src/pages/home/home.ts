import { HttpClient } from '@angular/common/http';
import { AdmobProvider } from './../../providers/admob/admob';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ModalController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  Gradients: any;

  constructor(

    public navCtrl: NavController,
    public platform: Platform,
    public admob: AdmobProvider,
    public http: HttpClient,
    public modal: ModalController,

    public navParams: NavParams) {
    this.http.get('./assets/data/gradient.json').subscribe((grad) => {
      this.Gradients = grad
    })

    platform.ready().then(() => {
      if (platform.is('android')) {
        this.admob.prepareBanner();
        this.admob.prepareInterstitial();
      }

    })
  }

  ionViewDidLoad() {

  }

  openGradient(grad, css, i) {
    if (i % 3 == 0) {
      if (this.platform.is('android')) {
        this.admob.showInterstitial()
      }
    }
    let modal = this.modal.create('GradientPage', { grad: grad, css: css })
    modal.present()
  }

  getBg(i) {
    let css = this.Gradients[i].css;
    let style = "{'" + css.replace(/:\s*/g, "':'").replace(/;\s*/g, "','").trim() + '}'
    let json = (style.slice(0, -3) + '}').replace(/'\s*/g, '"')
    return JSON.parse(json)
  }




}
