import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GradientPage } from './gradient';

@NgModule({
  declarations: [
    GradientPage,
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(GradientPage),
  ],
})
export class GradientPageModule {}
