import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, Platform } from 'ionic-angular';
import { Clipboard } from '@ionic-native/clipboard';
import { Toast } from '@ionic-native/toast';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';
import { Screenshot } from '@ionic-native/screenshot';

declare var cssbeautify;
@IonicPage()
@Component({
  selector: 'page-gradient',
  templateUrl: 'gradient.html',
})
export class GradientPage {
  grad: any;
  css: any;
  pre: string;
  SS: boolean = false;
  constructor(
    public navCtrl: NavController,
    private clipboard: Clipboard,
    public view: ViewController,
    public platform:Platform,
    private toast: Toast,
    private info: ToastController,
    private androidFullScreen: AndroidFullScreen,
    private screenshot: Screenshot,
    public navParams: NavParams) {
    this.grad = navParams.get('grad');
    this.css = navParams.get('css');

    this.pre = '.gradient { \n' + this.grad.css + '\n}'

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GradientPage');
  }


  dismiss() {
    this.view.dismiss()
  }

  copy() {
    
    if(!this.platform.is('android')){
      document.addEventListener('copy', (e: ClipboardEvent) => {
        e.clipboardData.setData('text/plain', this.pre);
        e.preventDefault();
        document.removeEventListener('copy',()=>{});
      });
      document.execCommand('copy');
      this.showInfo('CSS Copied to clipboard!')
    }else{
      this.clipboard.copy(this.pre).then(() => {
        this.showToast(`CSS Copied to clipboard!`)
      })
    }
  }

  screenshoot() {
    this.SS = true;
    this.androidFullScreen.immersiveMode();
    setTimeout(() => {
      this.screenshot.save('png', 100, this.grad.title).then(
        () => {
          // Show 
          
          this.SS = false;
          this.androidFullScreen.showSystemUI();
          this.showToast(`Gradient saved to folder Pictures!`)
        },
        e => {
          console.log(e)
        }
      );
      // this.androidFullScreen.showSystemUI();
        
      
    },1000)

  }

  showToast(msg){
    this.toast.show(msg, '2000', 'center').subscribe((toast) => {
      console.log(toast)
    })
  }

  showInfo(msg){
    console.log('show indo')
    let toast = this.info.create({
      message: msg,
      duration: 2000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

}
